## Deploy to EC2 server from Jenkins Mutibranch Pipeline
___

- Install on Jenkins `SSH Agent` plugin
- Create a SSH type (`SSH Username with private key`) credentials for EC2 on Jenkins making use of the content of .pem file.

    ![SSH Username with private key](/images/ssh_creds.png)

- From Jenkinsfile commit hash caf61a51d610170685943b932016c21725299818 

    #!/usr/bin/env groovy

pipeline {
    agent any
    stages {
        stage("Init") {
            steps {
                script {
                    
                }
            }
        }
        stage("Test") {
            steps {
                script {
                    echo "Testing"

                }
            }
        }
        stage("Build Docker Image") {
            steps {
                script {
                    echo "Building Docker Image"

                }
            }
        }
        stage("Deploy") {
            steps {
                script {
                    echo "Deploying"

                }
            }
        }
    }   
}


Change Deploy section to 

COMPLETE pipeline to deploy to EC2


